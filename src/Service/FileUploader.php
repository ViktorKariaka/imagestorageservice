<?php

namespace App\Service;

use Gregwar\ImageBundle\ImageHandler;
use Gregwar\ImageBundle\Services\ImageHandling;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class FileUploader
 *
 * @package App\Service
 */
class FileUploader
{
    /**
     * @var string
     */
    private $targetDirectory;

    /**
     * @var string
     */
    private $allowedResolution;

    /** @var ImageHandling */
    private $imageHandling;

    /**
     * FileUploader constructor.
     *
     * @param string        $targetDirectory
     * @param string        $allowedResolution
     * @param ImageHandling $imageHandling
     */
    public function __construct(string $targetDirectory, string $allowedResolution, ImageHandling $imageHandling)
    {
        $this->targetDirectory   = $targetDirectory;
        $this->allowedResolution = $allowedResolution;
        $this->imageHandling     = $imageHandling;
    }

    /**
     * @param UploadedFile $file
     *
     * @return string
     * @throws \Exception
     */
    public function uploadFile(UploadedFile $file)
    {
        $uuid     = md5(time() . random_int(PHP_INT_MIN, PHP_INT_MAX));
        $fileName = $uuid . '.' . $file->guessExtension();
        $image    = $this->imageHandling->open($file->getRealPath());

        if ($this->hasImageAllowedResolution($image)) {
            $image
                ->cropResize($image->width(), $image->height())
                ->save($this->targetDirectory . $fileName);
        } else {
            list($maxWidth, $maxHeight) = explode('x', $this->allowedResolution);

            $image
                ->resize($maxWidth, $maxHeight)
                ->save($this->targetDirectory . $fileName);
        }

        return $uuid;
    }

    /**
     * @param ImageHandler $imageHandler
     *
     * @return bool
     */
    private function hasImageAllowedResolution(ImageHandler $imageHandler): bool
    {
        list($maxWidth, $maxHeight) = explode('x', $this->allowedResolution);

        return ($imageHandler->width() <= $maxWidth) && ($imageHandler->height() <= $maxHeight);
    }
}
