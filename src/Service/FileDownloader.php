<?php

namespace App\Service;

use App\Helper\ImageResize;
use Gregwar\Image\Image;
use Gregwar\ImageBundle\Services\ImageHandling;

/**
 * Class FileDownloader
 *
 * @package App\Service
 */
class FileDownloader
{
    /**
     * Image format
     */
    private const ALBUM_FORMAT = 'album';

    /**
     * Image format
     */
    private const PORTRAIT_FORMAT = 'portrait';

    /**
     * Background color of zoom cropped image
     */
    private const ZOOM_CROP_BACKGROUND_COLOR = 0xffffff;

    /** @var array */
    private $thumbnailAllowedResolutions;

    /** @var string */
    private $targetDirectory;

    /** @var ImageHandling */
    private $imageHandling;

    /**
     * FileDownloader constructor.
     *
     * @param array         $thumbnailAllowedResolutions
     * @param ImageHandling $imageHandling
     * @param string        $targetDirectory
     */
    public function __construct(
        array $thumbnailAllowedResolutions,
        ImageHandling $imageHandling,
        string $targetDirectory
    )
    {
        $this->thumbnailAllowedResolutions = $thumbnailAllowedResolutions;
        $this->imageHandling               = $imageHandling;
        $this->targetDirectory             = $targetDirectory;
    }

    /**
     * @return string
     */
    public function getTargetDirectory(): string
    {
        return $this->targetDirectory;
    }

    /**
     * @param string $fileName
     * @param string $resolution
     *
     * @return Image
     */
    public function generateThumbnail(string $fileName, string $resolution): Image
    {
        $image = $this->imageHandling->open($this->targetDirectory . $fileName);
        list($requestedWidth, $requestedHeight) = explode('x', $resolution);

        $originalWidth  = $image->width();
        $originalHeight = $image->height();

        if ($this->isOriginResolutionEqualToRequested(
            $originalWidth,
            $originalHeight,
            $requestedWidth,
            $requestedHeight)
        ) {
            return $image;
        }

        if ($this->isOriginResolutionBiggerThanRequested(
            $requestedWidth,
            $requestedHeight,
            $originalWidth,
            $originalHeight)
        ) {
            $x = ($requestedWidth - $originalWidth) / 2;
            $y = ($requestedHeight - $originalHeight) / 2;

            return $image->zoomCrop(
                $requestedWidth,
                $requestedHeight,
                self::ZOOM_CROP_BACKGROUND_COLOR,
                $x,
                $y
            );
        }
        
        $originalImageFormat      = $this->getImageFormat($originalWidth, $originalHeight);
        $requestedThumbnailFormat = $this->getImageFormat($requestedWidth, $requestedHeight);
        
        if ($originalImageFormat === $requestedThumbnailFormat) {
            $imageResize = $this->getImageResizeEqualFormat(
                $originalWidth,
                $originalHeight,
                $requestedWidth,
                $requestedHeight
            );
        } else {
            $imageResize = $this->getImageResizeDiffFormat(
                $originalWidth,
                $originalHeight,
                $requestedWidth,
                $requestedHeight
            );
        }

        $image->scaleResize($imageResize->getWidth(), $imageResize->getHeight());

        $x = ($imageResize->getWidth() - $requestedWidth) / 2;
        $y = ($imageResize->getHeight() - $requestedHeight) / 2;

        return $image->crop($x, $y, $requestedWidth, $requestedHeight);
    }

    /**
     * @param string $searchedFileName
     *
     * @return string|null
     */
    public function getFileName(string $searchedFileName)
    {
        $fileList = scandir($this->targetDirectory);

        foreach ($fileList as $file) {
            list($fileName) = explode('.', $file);

            if ($fileName === $searchedFileName) {
                return $file;
            }
        }

        return null;
    }

    /**
     * @param string $resolution
     *
     * @return bool
     */
    public function isAllowedResolution(string $resolution): bool
    {
        return in_array($resolution, $this->thumbnailAllowedResolutions);
    }

    /**
     * @param int $width
     * @param int $height
     *
     * @return string
     */
    private function getImageFormat(int $width, int $height): string
    {
        if ($width >= $height) {
            return self::ALBUM_FORMAT;
        }

        return self::PORTRAIT_FORMAT;
    }

    /**
     * @param int $originalWidth
     * @param int $originalHeight
     * @param int $requestedWidth
     * @param int $requestedHeight
     *
     * @return ImageResize
     */
    private function getImageResizeDiffFormat(
        int $originalWidth,
        int $originalHeight,
        int $requestedWidth,
        int $requestedHeight
    ): ImageResize
    {
        if ($originalWidth > $originalHeight) {
            return new ImageResize($originalWidth, $requestedHeight);
        } else {
            return new ImageResize($requestedWidth, $originalHeight);
        }
    }

    /**
     * @param int $originalWidth
     * @param int $originalHeight
     * @param int $requestedWidth
     * @param int $requestedHeight
     *
     * @return ImageResize
     */
    private function getImageResizeEqualFormat(
        int $originalWidth,
        int $originalHeight,
        int $requestedWidth,
        int $requestedHeight
    ): ImageResize
    {
        if ($originalWidth > $originalHeight) {
            return new ImageResize($originalHeight, $requestedWidth);
        } else {
            return new ImageResize($requestedHeight, $originalWidth);
        }
    }

    /**
     * @param int $originalWidth
     * @param int $originalHeight
     * @param int $requestedWidth
     * @param int $requestedHeight
     *
     * @return bool
     */
    private function isOriginResolutionBiggerThanRequested(
        int $originalWidth,
        int $originalHeight,
        int $requestedWidth,
        int $requestedHeight
    ): bool
    {
        return ($originalWidth > $requestedWidth) || ($originalHeight > $requestedHeight);
    }

    /**
     * @param int $originalWidth
     * @param int $originalHeight
     * @param int $requestedWidth
     * @param int $requestedHeight
     *
     * @return bool
     */
    private function isOriginResolutionEqualToRequested(
        int $originalWidth,
        int $originalHeight,
        int $requestedWidth,
        int $requestedHeight
    ): bool
    {
        return ($originalWidth === $requestedWidth) && ($originalHeight === $requestedHeight);
    }
}
