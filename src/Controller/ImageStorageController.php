<?php

namespace App\Controller;

use App\Service\FileDownloader;
use App\Service\FileUploader;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Psr\Log\LoggerInterface;

/**
 * Class ImageStorageController
 * @package App\Controller
 *
 * @View()
 */
class ImageStorageController extends FOSRestController
{
    /**
     * @var FileUploader
     */
    private $fileUploader;

    /**
     * @var FileDownloader
     */
    private $fileDownloader;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * ImageStorageController constructor.
     *
     * @param FileUploader    $fileUploader
     * @param FileDownloader  $fileDownloader
     * @param LoggerInterface $logger
     */
    public function __construct(FileUploader $fileUploader, FileDownloader $fileDownloader, LoggerInterface $logger)
    {
        $this->fileUploader   = $fileUploader;
        $this->fileDownloader = $fileDownloader;
        $this->logger         = $logger;
    }

    /**
     * @param string $fileUUID
     * @param string $resolution
     *
     * @return mixed
     */
    public function getImageResolutionAction(string $fileUUID, string $resolution)
    {
        $fileDownloader      = $this->fileDownloader;
        $fileName            = $fileDownloader->getFileName($fileUUID);
        $isAllowedResolution = $fileDownloader->isAllowedResolution($resolution);

        if (is_null($fileName)) {
            return new JsonResponse('File does not exists', Response::HTTP_FORBIDDEN);
        }

        if (!$isAllowedResolution) {
            return new JsonResponse('Resolution does not support', Response::HTTP_FORBIDDEN);
        }

        try {
            $thumbnail = $fileDownloader->generateThumbnail($fileName, $resolution);
            $response  = new Response($thumbnail->get($thumbnail->guessType(), 100));

            $response->headers->set('Content-Type', 'image/' . $thumbnail->guessType());

            $response->headers->set(
                'Content-Disposition',
                ResponseHeaderBag::DISPOSITION_INLINE . '; filename=' . $fileName
            );

            return $response;
        } catch (\Exception $exception) {
            $msg = $exception->getMessage();
            $this->logger->error($msg);

            return new Response($msg, Response::HTTP_FORBIDDEN);
        }
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @throws \Exception
     */
    public function postImageAction(Request $request): Response
    {
        /** @var UploadedFile[] $files */
        $files = $request->files->all();

        if (!is_array($files)) {
            return new JsonResponse('Nothing to upload', Response::HTTP_FORBIDDEN);
        }

        $uniqueIds = [];

        foreach ($files as $file) {
            $uniqueIds[] = $this->fileUploader->uploadFile($file);
        }

        return new JsonResponse($uniqueIds);
    }
}
