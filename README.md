# ImageStorageService

=========================================

##Install project:

```bash
composer install
```

Configure application in *config/services.yaml*

Run built-in web server:
```bash
php bin/console server:run
```

## RESTful URLs:
* POST /images
* GET  /images/{imageUUID}/resolutions/{resolution}

#####Also for testing you can use simple HTML form:
* GET /index

#####Pay attention that project requires PHP 7.1.3 or higher to run.